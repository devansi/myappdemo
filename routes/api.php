<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::get('/userdetail', 'App\Http\Controllers\Api\UserController@userDetail')->name('userDetail');

Route::post('/logins', 'App\Http\Controllers\Api\UserController@logins')->name('logins');

Route::post('/registers', 'App\Http\Controllers\Api\UserController@registers')->name('registers');

Route::post('/forgot-passwords', 'App\Http\Controllers\Api\UserController@forgot_passwords')->name('forgot_passwords');

Route::get('/eventdetail', 'App\Http\Controllers\Api\EventController@eventDetail')->name('eventDetail');

Route::post('/addevent', 'App\Http\Controllers\Api\EventController@storeEvent')->name('addevent');

Route::post('/updateevent', 'App\Http\Controllers\Api\EventController@updateEvent')->name('updateevent');

Route::post('/deleteevent', 'App\Http\Controllers\Api\EventController@deleteEvent')->name('deleteevent');