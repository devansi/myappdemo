<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::redirect('/','/login');
Route::view('/login', 'auth.login');

Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::group(['middleware' => ['auth','isadmin']],function(){

Route::get('/index', [
    'as' => 'index', 'uses' => 'App\Http\Controllers\Admin\UserController@index'
]);

});

Route::get('/create', [
    'as' => 'create', 'uses' => 'App\Http\Controllers\Admin\UserController@create'
]);

Route::post('/store', [
    'as' => 'store', 'uses' => 'App\Http\Controllers\Admin\UserController@store'
]);

Route::get('/editUser/{id}', [
    'as' => 'editUser', 'uses' => 'App\Http\Controllers\Admin\UserController@edit'
]);

Route::post('/updateUser/{id}', [
    'as' => 'updateUser', 'uses' => 'App\Http\Controllers\Admin\UserController@update'
]);

Route::get('/deleteUser/{id}', [
    'as' => 'deleteUser', 'uses' => 'App\Http\Controllers\Admin\UserController@destroy'
]);

Route::get('/viewUser/{id}', [
    'as' => 'viewUser', 'uses' => 'App\Http\Controllers\Admin\UserController@show'
]);

 Route::get('/password/resetpassword/{id}/{email}', [
    'as' => 'resetpassword', 'uses' => 'App\Http\Controllers\Admin\UserController@resetpassword'
]);

Route::post('/changepassword', [
    'as' => 'changepassword', 'uses' => 'App\Http\Controllers\Admin\UserController@changepassword'
]);

Route::get('/Event-Module', [
    'as' => 'Event-Module', 'uses' => 'App\Http\Controllers\Admin\EventController@index'
]);

Route::get('/CreateEvent', [
    'as' => 'CreateEvent', 'uses' => 'App\Http\Controllers\Admin\EventController@create'
]);

Route::post('/addEvent', [
    'as' => 'addEvent', 'uses' => 'App\Http\Controllers\Admin\EventController@store'
]);

Route::get('/deleteEvent/{id}', [
    'as' => 'deleteData', 'uses' => 'App\Http\Controllers\Admin\EventController@destroy'
]);

Route::get('/editEvent/{id}', [
    'as' => 'editData', 'uses' => 'App\Http\Controllers\Admin\EventController@edit'
]);

Route::post('/updateEvent/{id}/{imgid}', [
    'as' => 'editData', 'uses' => 'App\Http\Controllers\Admin\EventController@update'
]);

Route::get('/viewEvent/{id}', [
    'as' => 'viewEvent', 'uses' => 'App\Http\Controllers\Admin\EventController@show'
]);

Route::get('/userEvent', [
    'as' => 'userEvent', 'uses' => 'App\Http\Controllers\Admin\UserEventController@index'
]);

Route::post('/userEventDetail', [
    'as' => 'userEventDetail', 'uses' => 'App\Http\Controllers\Admin\UserEventController@store'
]);

Route::get('/eventUser/{id}', [
    'as' => 'eventUser', 'uses' => 'App\Http\Controllers\Admin\EventController@display'
]);

Route::get('/Dashboard', [
    'as' => 'Dashboard', 'uses' => 'App\Http\Controllers\Admin\DashboardController@index'
]);




// User Login 

Route::group(['middleware' => ['auth','User']],function(){

    // return index page of user pannal.
    Route::get('/user-index', [
        'as' => 'user-index', 'uses' => 'App\Http\Controllers\Users\UserController@index'
    ]);


    Route::get('/User-Detail/{id}', [
        'as' => 'User-Detail', 'uses' => 'App\Http\Controllers\Users\UserController@view'
    ]);

    Route::get('/User-Edit/{id}', [
        'as' => 'User-Edit', 'uses' => 'App\Http\Controllers\Users\UserController@edit'
    ]);

    Route::post('/User-Update/{id}', [
        'as' => 'User-Update', 'uses' => 'App\Http\Controllers\Users\UserController@update'
    ]);

    Route::get('/User-Change-Password', [
        'as' => 'User-Change-Password', 'uses' => 'App\Http\Controllers\Users\UserController@ChangePassword'
    ]);

    Route::post('/User-Update-Password', [
        'as' => 'User-Update-Password', 'uses' => 'App\Http\Controllers\Users\UserController@UpdatePassword'
    ]);

});


// learning join query 

Route::get('/join', [
    'as' => 'join', 'uses' => 'App\Http\Controllers\Admin\UserEventController@joindemo'
]);

// Applying Laravel firstOrNew, firstOrCreate, firstOr, and updateOrCreate methods

Route::get('/form', [
    'as' => 'form', 'uses' => 'App\Http\Controllers\Demo\DemoController@index'
]);

Route::post('/storedemo', [
    'as' => 'storedemo', 'uses' => 'App\Http\Controllers\Demo\DemoController@store'
]);

Route::get('/formdata', [
    'as' => 'formdata', 'uses' => 'App\Http\Controllers\Demo\DemoController@indexform'
]);

Route::post('/addDemo', [
    'as' => 'addDemo', 'uses' => 'App\Http\Controllers\Demo\DemoController@add'
]);

Route::get('/demo', [
    'as' => 'demo', 'uses' => 'App\Http\Controllers\Demo\DemoController@demo'
]);

// check the raw query with join
Route::get('/rawDemo', [
    'as' => 'demo', 'uses' => 'App\Http\Controllers\Demo\DemoController@rawDemo'
]);


// Learning where, orWhere, orWhereHas, whereHas, wherebetween, orWhereBetween, whereNotBetween, orWhereNotBetween, whereIn, whereNotIn, orWhereIn, orWhereNotIn, whereNull, whereNotNull, orWhereNull, orWhereNotNull, whereDate / whereMonth / whereDay / whereYear / whereTime, whereColumn / orWhereColumn Query.

Route::get('/wherequery', [
    'as' => 'wherequery', 'uses' => 'App\Http\Controllers\Demo\QueryController@wherequery'
]);

Route::get('/orwherequery', [
    'as' => 'orwherequery', 'uses' => 'App\Http\Controllers\Demo\QueryController@orWherequery'
]);

Route::get('/whereHasquery', [
    'as' => 'whereHasquery', 'uses' => 'App\Http\Controllers\Demo\QueryController@whereHasquery'
]);

Route::get('/wherebetweenquery', [
    'as' => 'wherebetweenquery', 'uses' => 'App\Http\Controllers\Demo\QueryController@wherebetweenquery'
]);

Route::get('/whereInQuery', [
    'as' => 'whereInQuery', 'uses' => 'App\Http\Controllers\Demo\QueryController@whereInQuery'
]);

Route::get('/whereNullQuery', [
    'as' => 'whereNullQuery', 'uses' => 'App\Http\Controllers\Demo\QueryController@whereNullQuery'
]);

Route::get('/whereDateQuery', [
    'as' => 'whereDateQuery', 'uses' => 'App\Http\Controllers\Demo\QueryController@whereDateQuery'
]);

Route::get('/whereColumnQuery', [
    'as' => 'whereColumnQuery', 'uses' => 'App\Http\Controllers\Demo\QueryController@whereColumnQuery'
]);



// For PDF Download.
Route::get('/pdfdownload', [
    'as' => 'pdfdownload', 'uses' => 'App\Http\Controllers\Admin\DashboardController@pdfdownload'
]);

Route::get('/getDownloadTxt', [
    'as' => 'getDownloadTxt', 'uses' => 'App\Http\Controllers\Admin\DashboardController@getDownloadTxt'
]);


// Sent mail with attachment
Route::get('/sentMailForm',[
    'as' => 'sentMailForm' , 'uses' => 'App\Http\Controllers\Admin\UserController@Mailform'
]);

Route::post('/sentmail',[
    'as' => 'sentmail' , 'uses' => 'App\Http\Controllers\Admin\UserController@sentmail'
]);


//Invoice mail Event Activity module

Route::get('/EventActivityForm', [
    'as' => 'EventActivityForm', 'uses' => 'App\Http\Controllers\Users\UserEventActivityController@index'
]);

Route::get('/EventActivity/{event_id}', [
    'as' => 'EventActivity', 'uses' => 'App\Http\Controllers\Users\UserEventActivityController@getdropdowndata'
]);

Route::get('/EventActivityPrice/{event_id}', [
    'as' => 'EventActivityPrice', 'uses' => 'App\Http\Controllers\Users\UserEventActivityController@getdropdowndataprice'
]);

Route::post('/userattendy', [
    'as' => 'userattendy', 'uses' => 'App\Http\Controllers\Users\UserEventActivityController@store'
]);

//Timeslot Module

Route::get('/Timeslot',[
    'as' => 'Timeslot', 'uses' => 'App\Http\Controllers\Users\UserEventActivityController@TimeslotForm'
]);

Route::post('/StoreTimeslot', [
    'as' => 'StoreTimeslot', 'uses' => 'App\Http\Controllers\Users\UserEventActivityController@StoreTimeslot'
]);