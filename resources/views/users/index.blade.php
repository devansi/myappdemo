@extends('layouts.usermaster')
@section('content')

<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
       @if(count($errors) > 0)
       <div class="alert alert-danger">
        <ul>
          @foreach($errors->all() as $error)
          <li>{{$error}}</li>
          @endforeach
        </ul>
      </div>
      @endif
      @if(\Session::has('success'))
      <div class="alert alert-success" id="alertMessage">
        <p>{{\Session::get('success')}}</p>
      </div>
      @endif
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"> {{ Auth::user()->name }} Details</h3> 
        </div>
        <div class="card-body table-responsive p-0">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Action</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Action</th>
              </tr>
            </tfoot>
            <tbody>

              <tr>
                <td>{{ Auth::user()->id }}</td>
                <td>{{ Auth::user()->name }}</td>
                <td>{{ Auth::user()->email }}</td>
                <td><a href="{{url('/User-Detail/'.Auth::user()->id)}}" class="btn btn-primary">View</a>
                  <a href="{{url('/User-Edit/'.Auth::user()->id)}}" class="btn btn-info">Edit</a>
                </td>
              </tr>

            </tbody>
          </table>
        </div>
      </div>
      </div>
    </div>
  </div>
</section>
@endsection
