<style>
.table th img, .jsgrid .jsgrid-table th img, .table td img, .jsgrid .jsgrid-table td img {
    width: 100px!important;
    height: 100px!important;
     border-radius: unset!important; 
}
    table{
        font-size: 10px;
    }
    hr{
        height:2px;border:none;color:#333;background-color:#333;
    }
    .bold{
        font-weight: bold;
    }
    hr.hr1{
    }
    table.maintable{
        width: 100%;
    }
    .dark{
        background-color: #333;
        padding: 5px;
        color: #fff;
    }
    td.bottom-line{
        border-bottom: 3px solid #000;        
    }
    td.bottom-line-3{
        border-bottom: 2px solid #000;
    }


    table.item-table {
        /*background-color: #eee;*/
        width: 100%;
    }
    .item-table th {
        font-size: 10px;
    }
    .item-table td {
        font-size: 10px;
    }
    table.item-table tr td {
        /*background-color: #fff;*/
    }
    .full-border{
        border: 2px solid #000;
        margin: 0;
        padding: 0;
    }
    span.high-text{
        font-size: 12px;
    }

    .box-td{
        /*        text-align: center;
                background-color: #e3e3e3;
                border-top: 2px solid #000;
                border-bottom: 3px solid #000;*/
    }    
    .box-td h3{
        margin: 0;
    }

    .border-top{
        border-top: 2px solid #000;
    }
    .border-top-sm{
        border-top: 1px solid #000;
    }
    .border-bottom{
        border-bottom: 2px solid #000;
    }
    .border-right{
        border-right: 1px solid #000;
    }

    .margin-none{
        margin: 0;   
    }


    h3.invoice-title  {
        text-align: center;
    }
   table.order-table{
    border-collapse: collapse;
    text-align: center!important;
}
    
    table {
    border-collapse: collapse!important;
}
</style>

<table class="maintable" style="border: 0px !important;width: 100%;">
    <tr style="text-align: center!important;">
        <td style="width: 100%;" style="text-align: center;">
             <img style="text-align: center;" src="http://3.7.41.47/yumfood/public/uploads/LogoHak.png" style="width: 200px;">
        </td>
    </tr>  

    <?php //$product_data = json_decode($data['place_order']['product_details'],true); ?>

        <td style="width: 55%;">
        <p style="font-size: 20px;">Hak: Order Placed</p>
        <p>Dear Admin,</p>
        <p>Following Order has been placed. </p>
        <p>Thank you,<br>Team Hak.</p>
          
            <table style="width: 100%;" >                
                <tr style="">
                    <td colspan="2" style="text-align: center;background-color: #E0E0E0;">
                        <!--<span class="high-text bg-gray" style="font-weight: bold;">RETAIL INVOICE</span>-->
                        <br />
                        <span style="font-size: 16px;font-weight: bold; margin-bottom: 10px;">ORDER PLACED</span>
                        <br />
                    </td>
                </tr>
                <tr><td colspan="2"><hr  /></td></tr>
                <tr>
                    <td class="part1">
                      <strong>ORDER NUMBER</strong> :  Order#{{ $data[0][7] }}<br />
                    </td>
                    <td class="part2"> 
                        <strong>DATE</strong> : {{ $data[0][8] }} <br />
                    </td>
                </tr>
                <tr><td colspan="2"><hr  /></td></tr>
                <tr>
                    <td class="part1" style="background-color: #E0E0E0;">
                        <br />
                        <span class="high-text bold ">USER DETAILS</span>
                    </td>
                </tr>
                <tr>
                    <td class="part1" style="text-transform: uppercase;width: 49%;">
                        <br>
                       <strong>User Detail  </strong><br/>
                        <b>Name: {{ Auth::user()->name }} </b><br/>
                        <b>Email: {{ Auth::user()->email }} </b><br>
                        <!-- <b>Phone Number:  </b><br> -->
                      
                    </td>
                </tr>
                <tr><td colspan="2"></td></tr>
                <tr><td colspan="2"></td></tr>
                <tr>
                    <td colspan="2" class="">
                        <br />
                      
                        <table border= "1" class="item-table order-table" style="width: 100%;" >
                            <?php $counter = 0;?>
                            <tr style="border:1px solid #000;">
                                <td>Sr. No </td>
                                <td>Product Name</td>
                                <td>No of person</td>
                                <td>Item Per Price</td>
                                <td>Order Type</td>
                               <td colspan="2">Note</td>
                                
                            
                            </tr>
                            
                             
                            <tr>   
                               
                            <td><?php echo ++$counter; ?></td>
                            <td>{{ $data[0][0] }}</td>
                            <td>{{ $data[0][1] }}</td>
                            <td>{{ $data[0][2] }}</td>
                            <td>{{ $data[0][3] }}</td>
                            <td>-</td>                       
                            </tr>  
                            <tr>
                                <td style="text-align: right!important;" colspan="5">Subtotal</td>
                             
                                <td style="text-align: center;">
                                    {{ $data[0][4] }}
                                </td>
                            </tr>

                                
                            <tr>
                                <td style="text-align: right!important;" colspan="5">Coupon Discount</td>
                                <td style="text-align: center;">
                                  -
                                </td>
                            </tr>

                            
                           

                            <tr>
                                <td style="text-align: right!important;" colspan="5">Tax</td>
                                <td style="text-align: center;">
                                 {{ $data[0][6] }}
                                </td>
                            </tr>



                            <tr>
                                <td style="text-align: right!important;" colspan="5">Total</td>
                                <td style="text-align: center;">
                                   {{ $data[0][5] }}
                                </td>
                            </tr>
                        </table>
                        <br />
                    </td>
                </tr>
                <tr><td colspan="2"></td></tr>
                <tr><td colspan="2"></td></tr>
                <tr>
                   <td class="part1" style="text-transform: uppercase;width: 50%;">     
                        <b>Restaurant Name :- MyApp </b> <br>
                        <b>Address :- Bodakdev,Pakwan.</b> <br>
                    </td>
                    <td class="part2" style="text-transform: uppercase;width: 50%;float: right;">
                              
                    </td>    
                </tr>
                <tr>
                    <td colspan="2" class="" style="font-weight: bold;text-align: center;">                        
                       {{--  <br />THIS IS A COMPUTER GENERATED INVOICE AND DOES NOT REQUIRE SIGNATURE<br /> --}}
                    </td>
                </tr>
                <tr><td colspan="2"><hr  /></td></tr>
            </table>
        </td>
    </tr>
</table>

