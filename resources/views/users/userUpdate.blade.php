@extends('layouts.usermaster')
@section('content')
<div class="card">
    <div class="card-header">
        Edit {{ $user->name }}'s Profile
    </div>

    <div class="card-body">
        <form action="{{url('/User-Update/'.$user->id)}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="name">Name*</label>
                <input type="text" id="name" name="name" class="form-control" value="{{old('name') ?? $user->name}}" required>
            </div>
            <div class="form-group">
                <label for="email">Email*</label>
                <input type="email" id="email" name="email" class="form-control" value="{{old('email') ?? $user->email}}" required>
            </div>
            <div>
                <input class="btn btn-success" type="submit" value="Update">
                <a href="http://127.0.0.1:8000/user-index" class="btn btn-success">Back</a>
            </div>
        </form>
    </div>
</div>
@endsection