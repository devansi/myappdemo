@extends('layouts.usermaster')
@section('content')
<div class="card">
    <div class="card-header">
        User Events
    </div>
    @if(\Session::has('success'))
    <div class="alert alert-success" id="alertMessage">
        <p>{{\Session::get('success')}}</p>
    </div>
    @endif  
    <div class="card-body">
        <form action="userattendy" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="event">Event Name*</label>
                <select class="form-control" name="event" id="event" required>
                  <option >Select any one</option>
                  @foreach($eventname as $key => $row)
                  <option value="{{ $key }}">{{ $row }} </option>
                  @endforeach
              </select>
            </div>
            <div class="form-group">
                <label for="category">Activity Name*</label>
                <select class="form-control" name="activity" id="activity" required>
                   <option>Select any one</option>
                </select>
            </div>
            <div class="form-group">
                <label for="category">Number of person*</label>
                <input type="number" id="number" name="number" class="form-control input-number" value="1" min="1" max="50">
            </div>
            <div class="form-group" >
                <label for="category">Price*</label>
                <input type="text" id="price" name="price" class="form-control" >
            </div>
            <div>
                <input class="btn btn-success" type="submit" value="SAVE">
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

  $('#event').change(function() {
    var eventid = $('#event option:selected').val();
    $.ajax({
          url: "/EventActivity/" + eventid, 
          method: "get",
           success:function(data){
              $.each(data, function(data,value){

                $("#activity").append($("<option></option>").val(value.id).html(value.activity_name));  
            });  
        }
    });

});

$('#activity').change(function() {
    var activityid = $('#activity option:selected').val();
    $.ajax({
          url: "/EventActivityPrice/" + activityid, 
          method: "get",
           success:function(data){
            $.each(data, function(data,value){
                       $('#price').val(value.price);    
                    });
                
           }
    });
});


</script>
@endsection