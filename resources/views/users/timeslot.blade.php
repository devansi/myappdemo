@extends('layouts.usermaster')
@section('content')
<div class="card">
    <div class="card-header">
        User Events
    </div>
    @if(\Session::has('success'))
    <div class="alert alert-success" id="alertMessage">
        <p>{{\Session::get('success')}}</p>
    </div>
    @endif  
    <div class="card-body">
        <form action="StoreTimeslot" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="event">Event Name*</label>
                <select class="form-control" name="event" id="event" required>
                  <option >Select any one</option>
                  @foreach($eventname as $key => $row)
                  <option value="{{ $key }}">{{ $row }} </option>
                  @endforeach
              </select>
            </div>
            <div class="form-group">
                <label for="category">Activity Name*</label>
                <select class="form-control" name="activity" id="activity" required>
                   <option>Select any one</option>
                </select>
            </div>
            <div class="form-group">
                <div class="container" >
                 <div id="container"></div>
                   <div class='element' id='div_1' value="Monday" >
                      <div class="row pb-2" id='txt_1' >
                         <label for="image" class="col-sm-2 col-form-label font-weight-normal" name="day"> Monday </label>
                         <div class="col-sm-8" style="margin-left: -55px;">
                            <div  class="input-group timerange">
                               <input class="form-control" type="text" required name="timeslot[1]" placeholder="Time Slot">
                               <span class="input-group-addon pt-2 pl-2 " style="" >
                               <i aria-hidden="true" class="fa fa-calendar"></i>
                               </span>
                            </div>
                         </div>
                         <button data-repeater-delete  id="remove_1" type="button" class="btn btn-danger btn-sm icon-btn ml-2 mb-2 remove">
                         <i class="fa fa-minus"></i>
                         </button>
                      </div>
                   </div>   
                    <button data-repeater-delete type="button" class="btn btn-info add" id="plusbutton" > <i class="fa fa-plus"></i>
                      </button>
      
                </div>
              </div>
            <div>
                <input class="btn btn-success" type="submit" id="submit" value="SAVE">
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

  $('#event').change(function() {
    var eventid = $('#event option:selected').val();
    $.ajax({
          url: "/EventActivity/" + eventid, 
          method: "get",
           success:function(data){
              $.each(data, function(data,value){

                $("#activity").append($("<option></option>").val(value.id).html(value.activity_name));  
            });  
        }
    });

});



</script>

<script type="text/javascript">
   $(document).ready(function(){ 

   // Add new element
   var day_name = null;
   
   $(".add").click(function(){ 
   // Finding total number of elements added
   var total_element = $(".element").length; 
   // last <div> with element class id
   var lastid = $(".element:last").attr("id");
   var day = $(".element:last").attr("value"); 
   if(day == "Monday"){
     var array_day = []; 
      $( "div.element" ).each(function( index ) { 
           array_day.push($( this ).attr("value"));
       });
     console.log(array_day); 
     if(jQuery.inArray("Monday", array_day) === -1){
       day_name = "Monday";
       day_id = 1;
     }else if(jQuery.inArray("Tuesday", array_day) === -1){
       day_name = "Tuesday";
       day_id = 2;
     }else if(jQuery.inArray("Wednesday", array_day) === -1){
       day_name = "Wednesday";
       day_id = 3;
     }else if(jQuery.inArray("Thursday", array_day) === -1){
       day_name = "Thursday";
       day_id = 4;
     }else if(jQuery.inArray("Friday", array_day) === -1){
       day_name = "Friday";
       day_id = 5;
     }else if(jQuery.inArray("Saturday", array_day) === -1){
       day_name = "Saturday";
       day_id = 6;
     }else if(jQuery.inArray("Sunday", array_day) === -1){
       day_name = "Sunday";
       day_id = 7;
     }else{
       day_name = "";
     }
   }else if(day == "Tuesday"){   
     var array_day = []; 
      $( "div.element" ).each(function( index ) { 
           array_day.push($( this ).attr("value"));
       });
     console.log(array_day); 
     if(jQuery.inArray("Monday", array_day) === -1){
       day_name = "Monday";
       day_id = 1;
     }else if(jQuery.inArray("Tuesday", array_day) === -1){
       day_name = "Tuesday";
       day_id = 2;
     }else if(jQuery.inArray("Wednesday", array_day) === -1){
       day_name = "Wednesday";
       day_id = 3;
     }else if(jQuery.inArray("Thursday", array_day) === -1){
       day_name = "Thursday";
       day_id = 4;
     }else if(jQuery.inArray("Friday", array_day) === -1){
       day_name = "Friday";
       day_id = 5;
     }else if(jQuery.inArray("Saturday", array_day) === -1){
       day_name = "Saturday";
       day_id = 6;
     }else if(jQuery.inArray("Sunday", array_day) === -1){
       day_name = "Sunday";
       day_id = 7;
     }else{
       day_name = "";
     }
   }else if(day == "Wednesday"){
     var array_day = []; 
      $( "div.element" ).each(function( index ) { 
           array_day.push($( this ).attr("value"));
       });
     console.log(array_day); 
     if(jQuery.inArray("Monday", array_day) === -1){
       day_name = "Monday";
       day_id = 1;
     }else if(jQuery.inArray("Tuesday", array_day) === -1){
       day_name = "Tuesday";
       day_id = 2;
     }else if(jQuery.inArray("Wednesday", array_day) === -1){
       day_name = "Wednesday";
       day_id = 3;
     }else if(jQuery.inArray("Thursday", array_day) === -1){
       day_name = "Thursday";
       day_id = 4;
     }else if(jQuery.inArray("Friday", array_day) === -1){
       day_name = "Friday";
       day_id = 5;
     }else if(jQuery.inArray("Saturday", array_day) === -1){
       day_name = "Saturday";
       day_id = 6;
     }else if(jQuery.inArray("Sunday", array_day) === -1){
       day_name = "Sunday";
       day_id = 7;
     }else{
       day_name = "";
     }
   }else if(day == "Thursday"){
     var array_day = []; 
      $( "div.element" ).each(function( index ) { 
           array_day.push($( this ).attr("value"));
       }); 
     if(jQuery.inArray("Monday", array_day) === -1){
       day_name = "Monday";
       day_id = 1;
     }else if(jQuery.inArray("Tuesday", array_day) === -1){
       day_name = "Tuesday";
       day_id = 2;
     }else if(jQuery.inArray("Wednesday", array_day) === -1){
       day_name = "Wednesday";
       day_id = 3;
     }else if(jQuery.inArray("Thursday", array_day) === -1){
       day_name = "Thursday";
       day_id = 4;
     }else if(jQuery.inArray("Friday", array_day) === -1){
       day_name = "Friday";
       day_id = 5;
     }else if(jQuery.inArray("Saturday", array_day) === -1){
       day_name = "Saturday";
       day_id = 6;
     }else if(jQuery.inArray("Sunday", array_day) === -1){
       day_name = "Sunday";
       day_id = 7;
     }else{
       day_name = "";
     }
   }else if(day == "Friday"){
     var array_day = []; 
      $( "div.element" ).each(function( index ) { 
           array_day.push($( this ).attr("value"));
       });
     console.log(array_day); 
     if(jQuery.inArray("Monday", array_day) === -1){
       day_name = "Monday";
       day_id = 1;
     }else if(jQuery.inArray("Tuesday", array_day) === -1){
       day_name = "Tuesday";
       day_id = 2;
     }else if(jQuery.inArray("Wednesday", array_day) === -1){
       day_name = "Wednesday";
       day_id = 3;
     }else if(jQuery.inArray("Thursday", array_day) === -1){
       day_name = "Thursday";
       day_id = 4;
     }else if(jQuery.inArray("Friday", array_day) === -1){
       day_name = "Friday";
       day_id = 5;
     }else if(jQuery.inArray("Saturday", array_day) === -1){
       day_name = "Saturday";
       day_id = 6;
     }else if(jQuery.inArray("Sunday", array_day) === -1){
       day_name = "Sunday";
       day_id = 7;
     }else{
       day_name = "";
     }
   }else if(day == "Saturday"){
     var array_day = []; 
      $( "div.element" ).each(function( index ) { 
           array_day.push($( this ).attr("value"));
       });
     console.log(array_day); 
     if(jQuery.inArray("Monday", array_day) === -1){
       day_name = "Monday";
       day_id = 1;
     }else if(jQuery.inArray("Tuesday", array_day) === -1){
       day_name = "Tuesday";
       day_id = 2;
     }else if(jQuery.inArray("Wednesday", array_day) === -1){
       day_name = "Wednesday";
       day_id = 3;
     }else if(jQuery.inArray("Thursday", array_day) === -1){
       day_name = "Thursday";
       day_id = 4;
     }else if(jQuery.inArray("Friday", array_day) === -1){
       day_name = "Friday";
       day_id = 5;
     }else if(jQuery.inArray("Saturday", array_day) === -1){
       day_name = "Saturday";
       day_id = 6;
     }else if(jQuery.inArray("Sunday", array_day) === -1){
       day_name = "Sunday";
       day_id = 7;
     }else{
       day_name = "";
     }
   }else if(day == "Sunday"){
      var array_day = []; 
      $( "div.element" ).each(function( index ) { 
           array_day.push($( this ).attr("value"));
       });
     console.log(array_day); 
     if(jQuery.inArray("Monday", array_day) === -1){
       day_name = "Monday";
       day_id = 1;
     }else if(jQuery.inArray("Tuesday", array_day) === -1){
       day_name = "Tuesday";
       day_id = 2;
     }else if(jQuery.inArray("Wednesday", array_day) === -1){
       day_name = "Wednesday";
       day_id = 3;
     }else if(jQuery.inArray("Thursday", array_day) === -1){
       day_name = "Thursday";
       day_id = 4;
     }else if(jQuery.inArray("Friday", array_day) === -1){
       day_name = "Friday";
       day_id = 5;
     }else if(jQuery.inArray("Saturday", array_day) === -1){
       day_name = "Saturday";
       day_id = 6;
     }else if(jQuery.inArray("Sunday", array_day) === -1){
       day_name = "Sunday";
       day_id = 7;
     }else{
       day_name = "";
     }
   }else{
     
    var array_day = []; 
     var lastid = null;
      $( "div.element" ).each(function( index ) { 
           array_day.push($( this ).attr("value"));
       });
     console.log(array_day); 
     if(jQuery.inArray("Monday", array_day) === -1){
       day_name = "Monday";
       day_id = 1;

     }else if(jQuery.inArray("Tuesday", array_day) === -1){
       day_name = "Tuesday";
       day_id = 2;

     }else if(jQuery.inArray("Wednesday", array_day) === -1){
       day_name = "Wednesday";
       day_id = 3;
     }else if(jQuery.inArray("Thursday", array_day) === -1){
       day_name = "Thursday";
       day_id = 4;
     }else if(jQuery.inArray("Friday", array_day) === -1){
       day_name = "Friday";
       day_id = 5;
     }else if(jQuery.inArray("Saturday", array_day) === -1){
       day_name = "Saturday";
       day_id = 6;
     }else if(jQuery.inArray("Sunday", array_day) === -1){
       day_name = "Sunday";
       day_id = 7;
     }else{
       day_name = "";
     }
        
   
   
   }  
   if(lastid == null){    
      var nextindex = 1;
      $("#container").append("<div class='element' id='div_"+ nextindex +"' value='"+day_name+"'></div>");
      $("#div_" + nextindex).append("<div class='row pb-2' id='txt_"+ nextindex +"'><label class='col-sm-2 col-form-label font-weight-normal'>"+day_name+"</label><div class='col-sm-8' style='margin-left:-55px;'><div  class='input-group timerange'><input class='form-control' type='text' name='timeslot["+day_id+"]' placeholder='Time Slot' required='required'><span class='input-group-addon pt-2 pl-2'><i aria-hidden='true' class='fa fa-calendar'></i></span></div></div><button data-repeater-delete  id='remove_" + nextindex + "' type='button' class='btn btn-danger btn-sm icon-btn ml-2 mb-2 remove'><i class='fa fa-minus'></i></button></div>");
   }else{
      var split_id = lastid.split("_"); 
      var nextindex = Number(split_id[1]) + 1;
   }  
   var max = 7;
   // Check total number elements
   if(total_element < max && total_element != 0){
    // Adding new div container after last occurance of element class
    $(".element:last").after("<div class='element' id='div_"+ nextindex +"' value='"+day_name+"'></div>"); 
    // Adding element to <div>
    $("#div_" + nextindex).append("<div class='row pb-2' id='txt_"+ nextindex +"'><label class='col-sm-2 col-form-label font-weight-normal'>"+day_name+"</label><div class='col-sm-8' style='margin-left:-55px;'><div  class='input-group timerange'><input required class='form-control' type='text' name='timeslot["+day_id+"]' placeholder='Time Slot'><span class='input-group-addon pt-2 pl-2'><i aria-hidden='true' class='fa fa-calendar'></i></span></div></div><button data-repeater-delete  id='remove_" + nextindex + "' type='button' class='btn btn-danger btn-sm icon-btn ml-2 mb-2 remove'><i class='fa fa-minus'></i></button></div>");
      if(total_element == 6){
        $( "#plusbutton" ).hide();
      }else{
        $( "#plusbutton" ).show();
      }
   } 

   if( $(".container .element").length ==0 ){
    $("#submit").attr('disabled','disabled');
   } else {
    $("#submit").removeAttr('disabled');
   }
   });
   
   // Remove element
   $('.container').on('click','.remove',function(){
   
   var id = this.id;
   var split_id = id.split("_");
   var deleteindex = split_id[1];
   
   // Remove <div> with id
   $("#div_" + deleteindex).remove();
   $( "#plusbutton" ).show(); 
   if( $(".container .element").length ==0 ){
    $("#submit").attr('disabled','disabled');
   } else {
    $("#submit").removeAttr('disabled');
   }
   }); 
   });

   </script>
   <script type="text/javascript">
      $(document).on('click', '.timerange', function(e){ 
    e.stopPropagation();
    var input = $(this).find('input'); 
    var now = new Date();
    var hours = now.getHours();
    var period = "PM";
    if (hours < 12) {
      period = "AM";
    } else {
      hours = hours - 12;
    }
    var minutes = now.getMinutes();

    var range = {
      from: {
        hour: hours,
        minute: minutes,
        period: period
      },
      to: {
        hour: hours,
        minute: minutes,
        period: period
      }
    };

    if (input.val() !== "") {
      var timerange = input.val();
      var matches = timerange.match(/([0-9]{2}):([0-9]{2}) (\bAM\b|\bPM\b)-([0-9]{2}):([0-9]{2}) (\bAM\b|\bPM\b)/);
      if( matches.length === 7) {
        range = {
          from: {
            hour: matches[1],
            minute: matches[2],
            period: matches[3]
          },
          to: {
            hour: matches[4],
            minute: matches[5],
            period: matches[6]
          }
        }
      }
    };

     console.log(range);

    var html = '<div class="timerangepicker-container">'+
      '<div class="timerangepicker-from">'+
      '<label class="timerangepicker-label">From:</label>' +
      '<div class="timerangepicker-display hour">' +
          '<span class="increment fa fa-angle-up"></span>' +
          '<span class="value">'+('0' + range.from.hour).substr(-2)+'</span>' +
          '<span class="decrement fa fa-angle-down"></span>' +
      '</div>' +
      ':' +
      '<div class="timerangepicker-display minute">' +
          '<span class="increment fa fa-angle-up"></span>' +
          '<span class="value">'+('0' + range.from.minute).substr(-2)+'</span>' +
          '<span class="decrement fa fa-angle-down"></span>' +
      '</div>' +
      ':' +
      '<div class="timerangepicker-display period">' +
          '<span class="increment fa fa-angle-up"></span>' +
          '<span class="value">PM</span>' +
          '<span class="decrement fa fa-angle-down"></span>' +
      '</div>' +
      '</div>' +
      '<div class="timerangepicker-to">' +
      '<label class="timerangepicker-label">To:</label>' +
      '<div class="timerangepicker-display hour">' +
          '<span class="increment fa fa-angle-up"></span>' +
          '<span class="value">'+('0' + range.to.hour).substr(-2)+'</span>' +
          '<span class="decrement fa fa-angle-down"></span>' +
      '</div>' +
      ':' +
      '<div class="timerangepicker-display minute">' +
          '<span class="increment fa fa-angle-up"></span>' +
          '<span class="value">'+('0' + range.to.minute).substr(-2)+'</span>' +
          '<span class="decrement fa fa-angle-down"></span>' +
      '</div>' +
      ':' +
      '<div class="timerangepicker-display period">' +
          '<span class="increment fa fa-angle-up"></span>' +
          '<span class="value">PM</span>' +
          '<span class="decrement fa fa-angle-down"></span>' +
      '</div>' +
      '</div>' +
    '</div>';

    $(html).insertAfter(this);
    $('.timerangepicker-container').on(
      'click',
      '.timerangepicker-display.hour .increment',
      function(){
        var value = $(this).siblings('.value');
        value.text(
          increment(value.text(), 12, 1, 2)
        );
      }
    );

    $('.timerangepicker-container').on(
      'click',
      '.timerangepicker-display.hour .decrement',
      function(){
        var value = $(this).siblings('.value');
        value.text(
          decrement(value.text(), 12, 1, 2)
        );
      }
    );

    $('.timerangepicker-container').on(
      'click',
      '.timerangepicker-display.minute .increment',
      function(){
        var value = $(this).siblings('.value');
        value.text(
          increment(value.text(), 59, 0 , 2)
        );
      }
    );

    $('.timerangepicker-container').on(
      'click',
      '.timerangepicker-display.minute .decrement',
      function(){
        var value = $(this).siblings('.value');
        value.text(
          decrement(value.text(), 59, 0 , 2)
        );
      }
    );

    $('.timerangepicker-container').on(
      'click',
      '.timerangepicker-display.period .increment, .timerangepicker-display.period .decrement',
      function(){
        var value = $(this).siblings('.value');
        var next = value.text() == "PM" ? "AM" : "PM";
        value.text(next);
      }
    );
}); 

    $('.timerange').on('click', function(e) { 

    e.stopPropagation();
     if(($(".input-group timerange").next().hasClass("timerangepicker-container"))){ 
     }else{
      $("div.timerangepicker-container").remove();
     }

    var input = $(this).find('input');

    var now = new Date();
    var hours = now.getHours();
    var period = "PM";
    if (hours < 12) {
      period = "AM";
    } else {
      hours = hours - 12;
    }
    var minutes = now.getMinutes();

    var range = {
      from: {
        hour: hours,
        minute: minutes,
        period: period
      },
      to: {
        hour: hours,
        minute: minutes,
        period: period
      }
    };

    if (input.val() !== "") {
      var timerange = input.val();
      var matches = timerange.match(/([0-9]{2}):([0-9]{2}) (\bAM\b|\bPM\b)-([0-9]{2}):([0-9]{2}) (\bAM\b|\bPM\b)/);
      if( matches.length === 7) {
        range = {
          from: {
            hour: matches[1],
            minute: matches[2],
            period: matches[3]
          },
          to: {
            hour: matches[4],
            minute: matches[5],
            period: matches[6]
          }
        }
      }
    };
    console.log(range);

    var html = '<div class="timerangepicker-container">'+
      '<div class="timerangepicker-from">'+
      '<label class="timerangepicker-label">From:</label>' +
      '<div class="timerangepicker-display hour">' +
          '<span class="increment fa fa-angle-up"></span>' +
          '<span class="value">'+('0' + range.from.hour).substr(-2)+'</span>' +
          '<span class="decrement fa fa-angle-down"></span>' +
      '</div>' +
      ':' +
      '<div class="timerangepicker-display minute">' +
          '<span class="increment fa fa-angle-up"></span>' +
          '<span class="value">'+('0' + range.from.minute).substr(-2)+'</span>' +
          '<span class="decrement fa fa-angle-down"></span>' +
      '</div>' +
      ':' +
      '<div class="timerangepicker-display period">' +
          '<span class="increment fa fa-angle-up"></span>' +
          '<span class="value">PM</span>' +
          '<span class="decrement fa fa-angle-down"></span>' +
      '</div>' +
      '</div>' +
      '<div class="timerangepicker-to">' +
      '<label class="timerangepicker-label">To:</label>' +
      '<div class="timerangepicker-display hour">' +
          '<span class="increment fa fa-angle-up"></span>' +
          '<span class="value">'+('0' + range.to.hour).substr(-2)+'</span>' +
          '<span class="decrement fa fa-angle-down"></span>' +
      '</div>' +
      ':' +
      '<div class="timerangepicker-display minute">' +
          '<span class="increment fa fa-angle-up"></span>' +
          '<span class="value">'+('0' + range.to.minute).substr(-2)+'</span>' +
          '<span class="decrement fa fa-angle-down"></span>' +
      '</div>' +
      ':' +
      '<div class="timerangepicker-display period">' +
          '<span class="increment fa fa-angle-up"></span>' +
          '<span class="value">PM</span>' +
          '<span class="decrement fa fa-angle-down"></span>' +
      '</div>' +
      '</div>' +
    '</div>';

    $(html).insertAfter(this);
    $('.timerangepicker-container').on(
      'click',
      '.timerangepicker-display.hour .increment',
      function(){
        var value = $(this).siblings('.value');
        value.text(
          increment(value.text(), 12, 1, 2)
        );
      }
    );

    $('.timerangepicker-container').on(
      'click',
      '.timerangepicker-display.hour .decrement',
      function(){
        var value = $(this).siblings('.value');
        value.text(
          decrement(value.text(), 12, 1, 2)
        );
      }
    );

    $('.timerangepicker-container').on(
      'click',
      '.timerangepicker-display.minute .increment',
      function(){
        var value = $(this).siblings('.value');
        value.text(
          increment(value.text(), 59, 0 , 2)
        );
      }
    );

    $('.timerangepicker-container').on(
      'click',
      '.timerangepicker-display.minute .decrement',
      function(){
        var value = $(this).siblings('.value');
        value.text(
          decrement(value.text(), 59, 0, 2)
        );
      }
    );

    $('.timerangepicker-container').on(
      'click',
      '.timerangepicker-display.period .increment, .timerangepicker-display.period .decrement',
      function(){
        var value = $(this).siblings('.value');
        var next = value.text() == "PM" ? "AM" : "PM";
        value.text(next);
      }
    );

  });

  $(document).on('click', e => {

    if(!$(e.target).closest('.timerangepicker-container').length) {
      if($('.timerangepicker-container').is(":visible")) {
        var timerangeContainer = $('.timerangepicker-container');
        if(timerangeContainer.length > 0) {
          var timeRange = {
            from: {
              hour: timerangeContainer.find('.value')[0].innerText,
              minute: timerangeContainer.find('.value')[1].innerText,
              period: timerangeContainer.find('.value')[2].innerText
            },
            to: {
              hour: timerangeContainer.find('.value')[3].innerText,
              minute: timerangeContainer.find('.value')[4].innerText,
              period: timerangeContainer.find('.value')[5].innerText
            },
          };

          timerangeContainer.parent().find('input').val(
            timeRange.from.hour+":"+
            timeRange.from.minute+" "+    
            timeRange.from.period+"-"+
            timeRange.to.hour+":"+
            timeRange.to.minute+" "+
            timeRange.to.period
          );
          timerangeContainer.remove();
        }
      }
    }
    
  });

  function increment(value, max, min, size) {
    var intValue = parseInt(value);
    if (intValue == max) {
      return ('0' + min).substr(-size);
    } else {
      var next = intValue + 1;
      return ('0' + next).substr(-size);
    }
  }

  function decrement(value, max, min, size) {
    var intValue = parseInt(value);
    if (intValue == min) {
      return ('0' + max).substr(-size);
    } else {
      var next = intValue - 1;
      return ('0' + next).substr(-size);
    }
  } 

   </script>
   <style type="text/css">
  
.timerangepicker-container {
  display:flex;
  position: absolute;
}
.timerangepicker-label {
  display: block;
  line-height: 2em;
  background-color: #c8c8c880;
  padding-left: 1em;
  border-bottom: 1px solid grey;
  margin-bottom: 0.75em;
}

.timerangepicker-from,
.timerangepicker-to {
  border: 1px solid grey;
  padding-bottom: 0.75em;
  z-index: 99999;
    background: white;
}
.timerangepicker-from {
  border-right: none;
}
.timerangepicker-display {
  box-sizing: border-box;
  display: inline-block;
  width: 2.5em;
  height: 2.5em;
  border: 1px solid grey;
  line-height: 2.5em;
  text-align: center;
  position: relative;
  margin: 1em 0.175em;
}
.timerangepicker-display .increment,
.timerangepicker-display .decrement {
  cursor: pointer;
  position: absolute;
  font-size: 1.5em;
  width: 1.5em;
  text-align: center;
  left: 0;
}

.timerangepicker-display .increment {
  margin-top: -0.25em;
  top: -1em;
}

.timerangepicker-display .decrement {
  margin-bottom: -0.25em;
  bottom: -1em;
}

.timerangepicker-display.hour {
  margin-left: 1em;
}
.timerangepicker-display.period {
  margin-right: 1em;
}
</style>
@endsection