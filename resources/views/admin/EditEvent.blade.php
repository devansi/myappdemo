@extends('layouts.master')
@section('content')
<div class="card">
    <div class="card-header">
        Edit Event
    </div>
    <div class="card-body">
    	<form action="{{url('/updateEvent/'.$id->id.'/'.$id->image)}}" method="post" enctype="multipart/form-data" >
            @csrf
            <div class="form-group">
                <label for="name">Event Name*</label>
                <input type="text" id="event_name" name="event_name" class="form-control" value="{{old('event_name') ?? $id->event_name}}" required>
            </div>
            <div class="form-group">
                <label for="description">Description*</label>
                <textarea name="description" class="form-control" id="description" rows="5" required>{{old('description') ?? $id->description}}</textarea>
            </div>
            <div class="form-group">
                <label for="number">Phone number*</label>
                <input type="text" id="number" name="number" class="form-control" value="{{old('number') ?? $id->number}}" required min="10">
            </div> 
            <div class="form-group">
                <label for="address">Address*</label>
                <input type="text" id="Address" name="Address" class="form-control" value="{{old('Address') ?? $id->Address}}" required>
            </div>
            <iframe onload="getLatLong()" src="/" style="display: none;"></iframe>
            <div id="googleMap" style="width:100%;height:400px;"></div>
           
            <div class="form-group">
                <label for="image">Image*</label>
                <input type="file" name="image" id="image" class="form-control" value="{{old('image') ?? $id->image}}" >
              <img src="{{$id->image}}" hidden="img">
            </div>
            <div class="form-group">
                <label for="category">Category*</label>
                <select class="form-control" name="category" required>
                	<option>{{old('category') ?? $id->category}}</option>
                    <option>Culture</option>
                    <option>Sports</option>
                    <option>Fun</option>
                    <option>Education</option>
            </div>
            <div>
               <input class="btn btn-success" type="submit" value="Update">
                <a href="http://127.0.0.1:8000/Event-Module" class="btn btn-success">Back</a>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    document.getElementById("Address").onkeyup = function() {getLatLong()};
    function getLatLong() 
    {
           
        var Address = document.getElementById("Address").value;
     
        if (Address != ''){
      
            var geocoder = new google.maps.Geocoder();
         
            geocoder.geocode({ 'address': Address }, function (results, status) {
               
                if (status == google.maps.GeocoderStatus.OK) {
                   
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();
                    
                   myMap(latitude, longitude);
                     } else {
                    // alert("Request failed.")
                }
            });
        }
          
    }
             
</script>
    
<script>
function myMap(latitude, longitude)
{
    var mapProp= {
      center:new google.maps.LatLng(latitude,longitude),
      zoom:15,
    };

    var marker = new google.maps.Marker({
    position: new google.maps.LatLng(latitude,longitude),
    map: map,
    animation:google.maps.Animation.BOUNCE
    });

    var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
    marker.setMap(map);
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBaxzBNY7SnvSUKpV5x0vPY0x0iVq7L3CQ&libraries=geometry,places&callback=myMap"></script>

@endsection