@extends('layouts.master')
@section('content')
    <div class="card">
      	<div class="card-header">
       	 	<h3 class="card-title">Event Details</h3> 
     	  </div>
      	<div class="card-body table-responsive p-0">
          	<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
             	<tr>
    				    <th width="20%">Event Name:</th>
    				    <td>{{ $id->event_name }}</td>
    				  </tr>
				      <tr>
    				    <th>Description:</th>
    				    <td>{{ $id->description }}</td>
    			    </tr>
              <tr>
                <th>Contact Number:</th>
                <td>{{ $id->number }}</td>
              </tr>
              <tr>
                <th>Address:</th>
                <td>{{ $id->Address }}</td>
              </tr>
              <tr>
                <th>Image:</th>
                <td><img src="{{asset('uploads/event/'.$id['image'])}}" alt="Image Not Found" height="100px" width="100px"></td>
              </tr>
              <tr>
                <th>category:</th>
                <td>{{ $id->category }}</td>
              </tr>
            </table>
      	</div>
    </div>
    <div>
        <a href="http://127.0.0.1:8000/Event-Module" class="btn btn-success">Back</a>
  	</div>
@endsection



