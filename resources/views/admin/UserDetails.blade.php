@extends('layouts.master')
@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">User Details</h3> 
  </div>
  <div class="card-body table-responsive p-0">
   <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
      <tr>
        <th width="20%">Name:</th>
        <td>{{$id->name}}</td>
      </tr>
      <tr>
        <th>Email:</th>
        <td>{{$id->email}}</td>
      </tr>
    </table>
  </div>
</div>
<div>
  <a href="http://127.0.0.1:8000/index" class="btn btn-success">Back</a>
</div>
@endsection



