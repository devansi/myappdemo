@extends('layouts.master')
@section('content')

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div style="margin-bottom: 10px;" class="row">
                <div class="col-lg-12">
                    <a class="btn btn-success" href="CreateEvent">
                        Add Event
                    </a>
                </div>
            </div>
            @if(\Session::has('success'))
            <div class="alert alert-success" id="alertMessage">
                <p>{{\Session::get('success')}}</p>
            </div>
            @endif  
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Event Details</h3> 
              </div>
              <div class="card-body table-responsive p-0">
                  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Event Name</th>
                      <th>Description</th>
                      <th>Contact Number</th>
                      <th>Address</th>
                      <th>Image</th>
                      <th>category</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                    <th>Event Name</th>
                    <th>Description</th>
                    <th>Contact Number</th>
                    <th>Address</th>
                    <th>Image</th>
                    <th>category</th>
                    <th>Action</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    @foreach($eventmodule as $row)
                    <tr>
                      <td>{{ $row['event_name'] }}</td>
                      <td >{{ $row['description'] }}</td>
                      <td>{{ $row['number'] }}</td>
                      <td>{{ $row['Address'] }}</td>
                      <td><img src="{{asset('uploads/event/'.$row['image'])}}" alt="Image Not Found" height="100px" width="100px"></td>
                      <td>{{ $row['category'] }}</td>
                      <td>
                          <a href="{{url('/viewEvent/'.$row['id'])}}" class="btn btn-primary"  style="padding: 6px;">View</a>
                          <a href="{{url('/editEvent/'.$row['id'])}}" class="btn btn-info">Edit</a>
                          <a href="{{url('/deleteEvent/'.$row['id'])}}" class="btn btn-danger" style="margin: 4px;width: 96px">Delete</a>
                        <!--   <a href="{{url('/eventUser/'.$row['id'])}}" class="btn btn-success" style="margin: 4px;width: 96px">View User</a> -->
                           <a href="{{url('/eventUser/'.$row['id'])}}" class="btn btn-success viewuser" data-id="{{ $row['id'] }}" data-toggle="modal" data-target="#myModal"style="margin: 4px;width: 96px">View User</a>
                      </td>
                      
                    </tr>
                  @endforeach

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
       <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4>Name</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" id="datauser">
         
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  <script type="text/javascript">
    
    $(document).ready(function () {

            $(".viewuser").click(function (e) {
                var id = $(this).attr("data-id");
                 $.ajax({
                  url: "/eventUser/" + id, 
                  method: "get",  
                  success:function(data){
                    let name = '';
                    $.each(data, function(i,j){
                        name += j + "<br>";
                    });
                    $('#datauser').html(name);
                  }
              });
          
            });
           
    });
  </script>
@endsection
    
              