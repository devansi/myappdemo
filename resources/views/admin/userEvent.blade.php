@extends('layouts.master')
@section('content')
<div class="card">
    <div class="card-header">
        User Event
    </div>
    @if(\Session::has('success'))
    <div class="alert alert-success" id="alertMessage">
        <p>{{\Session::get('success')}}</p>
    </div>
    @endif  
    <div class="card-body">
        <form action="userEventDetail" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="category">Category*</label>
                <select class="form-control" name="Category" required>
                	<option >Select any one</option>
                  @foreach($eventmodule as $row)
                  <option value="{{ $row['id'] }}">{{ $row['event_name'] }} </option>
                  @endforeach
              </select>
            </div>
            <div class="form-group">
                <label for="category">Name*</label>
                <select class="form-control" name="name" placeholder="Select Any one" required>
                   <option>Select any one</option>
                   @foreach($user as $row)
                   <option value="{{ $row['id'] }}">{{ $row['name'] }}</option>
                   @endforeach
               </select>
            </div>
            <div>
                <input class="btn btn-success" type="submit" value="SAVE">
            </div>
        </form>
    </div>
</div>
@endsection