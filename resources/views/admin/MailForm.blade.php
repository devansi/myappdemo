@extends('layouts.master')
@section('content')
<div class="card">
    <div class="card-header">
        Event Module
    </div>
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif 
         @if(\Session::has('success'))
        <div class="alert alert-success" id="alertMessage">
          <p>{{\Session::get('success')}}</p>
        </div>
        @endif         
    <div class="card-body">
        <form action="sentmail" method="post" enctype="multipart/form-data" >
            @csrf
            <div class="form-group">
                <label for="description">Email*</label>
                <input type="text" id="email" name="email" class="form-control" value="" required>
            </div>
            <div class="form-group">
                <label for="number">Subject*</label>
                <input type="text" id="subject" name="subject" class="form-control" value="" required>
            </div> 
            <div class="form-group">
                <label for="address">Message*</label>
                <input type="text" id="message" name="message" class="form-control" value="" required>
            </div>
            <div class="form-group">
                <label for="image">Image*</label>
                <input type="file" name="image" id="image" class="form-control" required>
            </div>
            <div>
                <input class="btn btn-success" type="submit" value="Sent Mail" style="margin-top: 22px">
                <a href="http://127.0.0.1:8000/index" class="btn btn-success" style="margin-top: 22px">Back</a>
            </div>
        </form>
    </div>
</div>

@endsection
  
    
