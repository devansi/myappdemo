@extends('layouts.master')
@section('content')

<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div style="margin-bottom: 10px;" class="row">
          <div class="col-lg-12">
            <a class="btn btn-success" href="create">
              Add User
            </a>
          </div>
        </div>
         <div style="margin-bottom: 10px;" class="row">
          <div class="col-lg-12">
            <a class="btn btn-success" href="/sentMailForm">
              Sent mail with attachment
            </a>
          </div>
        </div>
        @if(count($errors) > 0)
        <div class="alert alert-danger">
          <ul>
            @foreach($errors->all() as $error)
            <li>{{$error}}</li>
            @endforeach
          </ul>
        </div>
        @endif
        @if(\Session::has('success'))
        <div class="alert alert-success" id="alertMessage">
          <p>{{\Session::get('success')}}</p>
        </div>
        @endif
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">User Details</h3> 
          </div>
          <div class="card-body table-responsive p-0">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Action</th>
                </tr>
              </tfoot>
              <tbody>
                @foreach($user as $row)
                <tr>
                  <td>{{ $row['id'] }}</td>
                  <td>{{ $row['name'] }}</td>
                  <td>{{ $row['email'] }}</td>
                  <td><a href="{{url('/viewUser/'.$row['id'])}}" class="btn btn-primary">View</a>
                    <a href="{{url('/editUser/'.$row['id'])}}" class="btn btn-info">Edit</a>
                    <a href="{{url('/deleteUser/'.$row['id'])}}" class="btn btn-danger">Delete</a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
