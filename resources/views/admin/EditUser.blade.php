@extends('layouts.master')
@section('content')
<div class="card">
    <div class="card-header">
        Edit User
    </div>

    <div class="card-body">
        <form action="{{url('/updateUser/'.$id->id)}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="name">Name*</label>
                <input type="text" id="name" name="name" class="form-control" value="{{old('name') ?? $id->name}}" onChange="Validate()" required>
            </div>
            <div class="form-group">
                <label for="email">Email*</label>
                <input type="email" id="email" name="email" class="form-control" value="{{old('email') ?? $id->email}}" required>
            </div>
            <div>
                <input class="btn btn-success" type="submit" value="Update">
                <a href="http://127.0.0.1:8000/index" class="btn btn-success">Back</a>
            </div>
        </form>
    </div>
</div>
@endsection