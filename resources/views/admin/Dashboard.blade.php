@extends('layouts.master')
@section('content')

<section class="content">
  <div class="container-fluid" >
  	<div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
    </div>
    <div class="row">
      <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
          <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                  <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Users</div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $user }}</div>
                </div>
                <div class="col-auto" style="margin-top: 11px;">
                  <i class="fas fa-user fa-2x text-gray-300"></i>
                </div>
              </div>
            </div>
          </div>
        </div>

       <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-success shadow h-100 py-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Total Events</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $eventcount }}</div>
              </div>
              <div class="col-auto" style="margin-top: 11px;">
                <i class="fas fa-calendar fa-2x text-gray-300"></i>
              </div>
            </div>
          </div>
        </div>
      </div>

       <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-info shadow h-100 py-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Total event user</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $userevent }}</div>
              </div>
              <div class="col-auto" style="margin-top: 11px;">
                <i class="fas fa-users fa-2x text-gray-300"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-warning shadow h-100 py-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
              <a href="{{url('/pdfdownload')}}" class="btn btn-primary btn-user btn-block">
                Download PDF
              </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>   <!-- row -->

    <div class="row">
      <!-- Area Chart -->
      <div class="col-xl-9 col-lg-9">
        <div class="card shadow mb-4">
          <!-- Card Header -->
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Event Overview Area Chart</h6>
          </div>
          <!-- Card Body -->
          <div class="card-body">
            <div class="chart-area">
            	<div id="myAreaChart" style="width: 600px; height: 300px"></div>
             
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-md-6 mb-4" style="height:10%">
        <div class="card border-left-secondary shadow h-100 py-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
              <a href="{{url('/getDownloadTxt')}}" class="btn btn-success btn-user btn-block">
                Download TXT
              </a>
              </div>
            </div>
          </div>
        </div>
      </div>
   </div>  

    <div class="row">
      <!-- Pie Chart -->
      <div class="col-xl-9 col-lg-9">
        <div class="card shadow mb-4">
          <!-- Card Header -->
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Event Overview Pie Chart</h6>
          </div>
          <!-- Card Body -->
          <div class="card-body">
            <div class="chart-area">
              <div id="piechart" style="width: 600px; height: 300px"></div>
             
            </div>
          </div>
        </div>
      </div>
    </div> 
    <div class="row">
      <!-- bar Chart -->
      <div class="col-xl-9 col-lg-9">
        <div class="card shadow mb-4">
          <!-- Card Header -->
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Event Overview Bar Chart</h6>
          </div>
          <!-- Card Body -->
          <div class="card-body">
            <div class="chart-area">
              <div id="barchart_material" style="width: 600px; height: 300px"></div>
             
            </div>
          </div>
        </div>
      </div>
    </div> 
   
  </div>
</section>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() 
  {
 
    var data = google.visualization.arrayToDataTable([
        ['Events' , 'Event Users' ],

          <?php
         
            foreach($linechartdata as $d) {
               echo "[$d],";   
            }
            ?>
    ]);

    var options = {
      title: 'Enroll Event Users',
      curveType: 'function',
      legend: { position: 'bottom' }
    };

    var chart = new google.visualization.LineChart(document.getElementById('myAreaChart'));

    chart.draw(data, options);
    }
</script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Events' , 'Event Users'],

                @php
                foreach($linechartdata as $d) {
               echo "[$d],";   
            }
                @endphp
        ]);

          var options = {
            title: 'Enroll Event Users',
            is3D: false,
          };

          var chart = new google.visualization.PieChart(document.getElementById('piechart'));

          chart.draw(data, options);
        }
      </script>
      <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Events' , 'Event Users'],

            @php
              foreach($linechartdata as $d) {
               echo "[$d],";   
            }
            @endphp
        ]);

        var options = {
          chart: {
            title: 'Enroll Event Users',
            subtitle: 'Count of user participate in events',
          },
          bars: 'vertical'
        };
        var chart = new google.charts.Bar(document.getElementById('barchart_material'));
        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
    </script>

@endsection