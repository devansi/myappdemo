<tr>
<td class="header">
<a href="http://127.0.0.1:8000" style="display: inline-block;">
@if (trim($slot) === 'Laravel')
<h1>MyApp</h1>
<!-- <img src="https://laravel.com/img/notification-logo.png" class="logo" alt="MyApp"> -->
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
