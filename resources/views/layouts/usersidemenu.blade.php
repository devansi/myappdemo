<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="http://127.0.0.1:8000/bower_components/admin-lte/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">My App</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="http://127.0.0.1:8000/bower_components/admin-lte/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"> {{ Auth::user()->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- for user profile dashboard -->
          <li class="nav-item">
            <a href="http://127.0.0.1:8000/user-index" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Profile
              </p>
            </a>
          </li>
          <!-- end of user profile dashboard -->
          <!-- for change password  -->
          <li class="nav-item">
            <a href="http://127.0.0.1:8000/User-Change-Password" class="nav-link">
              <i class="nav-icon fas fa-lock"></i>
              <p>
                Change Password
              </p>
            </a>
          </li>
          <!-- end of change password -->
          <!-- User Event Module -->
          <li class="nav-item">
            <a href="http://127.0.0.1:8000/EventActivityForm" class="nav-link">
              <i class="nav-icon fas fa-calendar"></i>
              <p>
                Events
              </p>
            </a>
          </li>
          <!-- End of User Event Module -->
           <!-- Timeslot Module -->
          <li class="nav-item">
            <a href="http://127.0.0.1:8000/Timeslot" class="nav-link">
              <i class="nav-icon fas fa-clock"></i>
              <p>
                Timeslot
              </p>
            </a>
          </li>
           <!-- Timeslot Module -->
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>