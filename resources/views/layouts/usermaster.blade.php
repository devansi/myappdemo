<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>AdminLTE 3 | Starter</title>
  <link rel="stylesheet" href="http://127.0.0.1:8000/bower_components/admin-lte/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="http://127.0.0.1:8000/bower_components/admin-lte/dist/css/adminlte.min.css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">  
  <link rel="stylesheet" href="http://127.0.0.1:8000/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="http://127.0.0.1:8000/css/sb-admin-2.min.css">
  

   <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
  <script type="text/javascript"> 
      $(function(){
    setTimeout(function(){
        $("#alertMessage").hide();
        }, 2000);
      });
    </script>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
      @include('layouts.header')
      @include('layouts.usersidemenu')
      <div class="content-wrapper">
         <div class="content-header">
          </div>
      @yield('content')
      </div>
</div>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script src="http://127.0.0.1:8000/bower_components/admin-lte/plugins/jquery/jquery.min.js"></script>
<script src="http://127.0.0.1:8000/bower_components/admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="http://127.0.0.1:8000/bower_components/admin-lte/dist/js/adminlte.min.js"></script>
<script src="http://127.0.0.1:8000/js/Chart.min.js"></script>
<script src="http://127.0.0.1:8000/js/jquery.dataTables.min.js"></script>
<script src="http://127.0.0.1:8000/js/datatables-demo.js"></script>
</body>
</html>