@extends('layouts.master')
@section('content')
<div class="card">
    <div class="card-header">
        Create User
    </div>
    @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(\Session::has('success'))
        <div class="alert alert-success" id="alertMessage">
          <p>{{\Session::get('success')}}</p>
        </div>
    @endif
    <div class="card-body">
        <form action="storedemo" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="name">Name*</label>
                <input type="text" id="name" name="name" class="form-control" value="" onChange="Validate()" required>
            </div>
            <div class="form-group">
                <label for="email">Email*</label>
                <input type="email" id="email" name="email" class="form-control" value="" required>
            </div>
            <div class="form-group">
                <label for="password">Password*</label>
                <input type="password" id="password" name="password" class="form-control" value="" required>
            </div>
            <div>
                <input class="btn btn-success" type="submit" value="SAVE">
            </div>
        </form>
    </div>
</div>
@endsection