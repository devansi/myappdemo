<?php
namespace App\Notifications;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use PDF;
class InvoiceMailSent extends Notification implements ShouldQueue
{
    use Queueable;
    protected $data;
    
    /**
    * Create a new notification instance.
    *
    * @return void
    */
    public function __construct(array $data)
    {
        $this->data = $data;
    }
    /**
    * Get the notification's delivery channels.
    *
    * @param  mixed  $notifiable
    * @return array
    */
    public function via($notifiable)
    {
        return ['mail'];
    }
    /**
    * Get the mail representation of the notification.
    *
    * @param  mixed  $notifiable
    * @return \Illuminate\Notifications\Messages\MailMessage
    */
    public function toMail($notifiable)
    {   
        $data = $this->data;
        $pdf = PDF::loadview('users.invoice', compact('data'));
        return (new MailMessage)
            ->view('users.invoice', ['data' => $this->data])
            ->attachData($pdf->output(), 'invoice.pdf', [
                    'mime' => 'application/pdf',
                    ]);
    }
/**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}