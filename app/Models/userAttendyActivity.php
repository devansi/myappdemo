<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class userAttendyActivity extends Authenticatable
{
		use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   protected $table = 'user_attendy_activity';
    protected $fillable = [
        'user_id','event_id','activity_id','number_of_person','total_price', 'tax'
    ];

   
}




