<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventModule extends Model
{
    // use HasFactory;
					
    protected $table = 'event_module';
    protected $fillable = [
    	'event_name','description','number','Address','image','category','latitude','longitude'
    ];


	   public function data()
	{
	    return $this->belongsTo('App\Models\UserEvent', 'event_id', 'id');
	}
}

