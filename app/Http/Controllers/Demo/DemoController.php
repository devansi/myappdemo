<?php

namespace App\Http\Controllers\Demo;
use App\Models\UserEvent;
use App\Models\User;
use App\Models\EventModule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Hash;
use Auth;

class DemoController extends Controller
{
	public function index()
	{
		return view('demo.form');
	}

	public function store(Request $request)
	{
		$user = User::firstOrNew(['email' =>  request('email')]);
		$user->name = request('name');
		$user->password = hash::make(request('password'));
		$user->save();
		return view('demo.form')->with('success','Data Inserted!');
	}

	public function indexform()
	{
		return view('demo.formdata');
	}

	public function add(Request $request)
	{

		// $user = User::where('email', request('email'))->firstOr( function () {
  //   	$user = User::create([
  //   		'email' =>  request('email'),
		//     'name' => request('name'),
		//     'password' => hash::make(request('password'))
  //   	]);
  //   	$user->save();
  //   	});
  //   	$user->name = request('name');
		// $user->password = hash::make(request('password'));
  //   	$user->save();




//Udate and create data
		// $user = User::updateOrCreate(
		// 	['email' =>  request('email')],
		//     ['name' => request('name'),
		//     'password' => hash::make(request('password'))]
		// );

		// if($user)
  //   	{
		//  	return (" update message" );
		// }else{
		//  	return(" error email id already exists");
		// }





//first or create
  //   	$user = User::firstOrCreate(
		//     ['email' => request('email')],
		//     ['name' => request('name'),'password' => hash::make(request('password'))]
		// );
  //   	if($user)
  //   	{
		//  	return (" success message" );
		// }else{
		//  	return(" error email id already exists");
		// }

		// return redirect('/formdata')->with('success','Data Inserted!');
	}

	public function Demo()
	{
		$id = Auth::user()->id;
		return $users = UserEvent::selectRaw('event_id,id')
                     ->where('user_id', '=', $id)
                     ->get();
	}	

	// using raw query with join

	public function rawDemo()
	{
		$id = Auth::user()->id;
		 return DB::table('users')	
				->join('user_event','user_event.user_id','users.id')	
   				->selectRaw('users.name')
   				->where('user_event.user_id', '1')
   				->get();		

		 
	}	

}