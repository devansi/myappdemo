<?php

namespace App\Http\Controllers\Demo;
use App\Models\UserEvent;
use App\Models\User;
use App\Models\EventModule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Hash;
use Auth;

class QueryController extends Controller
{

	public function wherequery()
	{	
		// Where query implement 
		// Here if i want some specific value thn i will use pluck insted of get
		return $user = User::where('id',6)->get();
	}

	// orWhere query implement 
	public function orWherequery()
	{
		// return $users = DB::table('users')
  //                   ->where('id', '>', 100)
  //                   ->orWhere('name', 'Devanshi')
  //                   ->get();

		// Here both condition display in output

        return $users = DB::table('users')
		            ->where('id', '>', 70)
		            ->orWhere(function($query) {
		                $query->where('name', 'Devanshi dave')
		                      ->where('id', '<', 50);
		            })
		            ->get();
	}


	public function whereHasquery()
	{
		// something problematic
		// return $users = User::has('id', '>=', 1)->get();
		return $users = User::whereHas('email', function ()
				{
				    $query->where('content', 'like', 'dev%');
				})->get();
	}

	public function wherebetweenquery()
	{
		// where Between query implementation
		// it is used for between some renge get output
		// whereNotBetween Query implementation
		// it is used for between some renge to not getting output


		// Here whereBetween and orWhereBetween Will be same
		// Here whereNotBetween and orWhereNotBetween both are same
		return $users = DB::table('users')
	           ->orwhereNotBetween('id', [1, 50])
	           ->orWhereBetween('id',[51,100])
	           ->get();
	}

	public function whereInQuery()
	{
		//whereIn / whereNotIn / orWhereIn / orWhereNotIn

		// Here whereIn and orWhereIn Will be same
		// Here whereNotIn and orWhereNotIn both are same

		return $users = User::whereIn('id', [6, 5, 33])
                    	->get();

        // return $users = User::orwhereIn('id', [6, 5, 33])
        //             	->get();

        // return $users = User::whereNotIn('id', [6, 5, 33])
        //             	->get();

        // return $users = User::orwhereNotIn('id', [6, 5, 33])
        //             	->get();
	}

	public function whereNullQuery()
	{
		//whereNull / whereNotNull / orWhereNull / orWhereNotNull

		// return $users = DB::table('users')
  //                   ->orwhereNull('user_token')
  //                   ->get();

  //       return $users = DB::table('users')
  //                   ->whereNull('user_token')
  //                   ->get();

        // return $users = DB::table('users')
        //             ->whereNotNull('id')
        //             ->get();

        return $users = DB::table('users')
                    ->orWhereNotNull('name')
                    ->get();
	}


	public function whereDateQuery()
	{
		//whereDate / whereMonth / whereDay / whereYear / whereTime

		// return $users = DB::table('users')
  //               ->whereDate('created_at', '2020-09-18')
  //               ->get();

		// return $users = DB::table('users')
  //               ->whereMonth('created_at', '09')
  //               ->get();

		// return $users = DB::table('users')
  //               ->whereDay('created_at', '18')
  //               ->get();

		// return $users = DB::table('users')
  //               ->whereYear('created_at', '2020')
  //               ->get();

		return $users = DB::table('users')
                ->whereTime('created_at', '06:36:14')
                ->get();
	}

	public function whereColumnQuery()
	{
		//whereColumn / orWhereColumn
		return $users = DB::table('users')
                ->orwhereColumn('updated_at', '>', 'created_at')
                ->get();
	}

}