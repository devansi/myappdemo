<?php

namespace App\Http\Controllers\Admin;
use App\Models\EventModule;
use App\Models\UserEvent;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
use Illuminate\Support\Facades\Hash;
use Auth;
use DB;
use Illuminate\Support\Arr;
use PDF;

class DashboardController extends Controller
{
	// For display user event map 
	// display count of user , count of event and count of userevent 
	// It is dashboard of admin
	public function index()
	{
		$user = User::all();
		$user = count($user);
		$event = EventModule::all();
		$eventcount = count($event);
		$userevent = UserEvent::all()->groupBy('user_id');
		$userevent = count($userevent);
		
		$dat = [];
		//query for user event count
		foreach ($event as $key => $value) 
		{
			$id [] = $value->id;
			$eventname[] = $value->event_name;
			//Get no of user from user table
			foreach ($id as $key => $value)
			 {
				$user_id = UserEvent::where('event_id',$value)->pluck('user_id');
	        	$user_detail = User::whereIn('id',$user_id)->get('name');
			}
	         	$eventuser[$key] = count($user_detail);
           
		}	
		//For line chart data event name and no of user
		$linechartdata = [];
		for($i = 0; $i < count($eventuser); $i++)
		{
			// $linechartdata = Arr::add($linechartdata,"'".$eventname[$i]."'" , $eventuser[$i]);
			$linechartdata = Arr::add($linechartdata,$i, "'".$eventname[$i]."',".$eventuser[$i]);
		}

 		return view('admin.dashboard',compact('user','eventcount','userevent','linechartdata'));
	}


	public function pdfdownload()
	{
		$user = User::all();
		$event = EventModule::all();
		$data = [
			'title' => Auth::user()->name,
			'name' => count($user),
			'event' => count($event)

		];
        $pdf = PDF::loadView('pdf', $data);
  
        return $pdf->download('MyApp.pdf');
	}



	public function getDownloadTxt(Request $request) {
    // prepare content
    $user = User::all();
    $content = "Name of Users which is registered in our website \n";
    foreach ($user as $users) {
      $content .= $users->name;
      $content .= "\n";
    }

    // file name that will be used in the download
    $fileName = "MyApp.txt";

    // use headers in order to generate the download
    $headers = [
      'Content-type' => 'text/plain', 
      'Content-Disposition' => sprintf('attachment; filename="%s"', $fileName),
      'Content-Length' => strlen($content)
    ];

    // make a response, with the content, a 200 response code and the headers
    return Response::make($content, 200, $headers);
}

}