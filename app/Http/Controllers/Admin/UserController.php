<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Auth;
use Mail;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // User Module.
    public function index()
    {
       if (Auth::check())
        {
           $user = User::all()->toArray();
           return view('admin.index', compact('user'));
        }
        else
        {
        return redirect('/login');
        } 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    // Create user form.
    public function create()
    {
         return view('admin.CreateUser');//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    // Store user data in database.

    public function store(Request $request)
    {     
        $request['userType'] = "2";
        $this->validate($request , [
             'name' => 'required',
             'email' => 'required',
             'password' => ['required', 'string', 'min:8'],
            ]);
        
        $CreateUser = new User([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request['password']),
            'userType' => $request['userType'],
            ]);
        $CreateUser->save();
        return redirect()->route('index')->with('success','Data Added');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    // display user's Details
    public function show($id)
    {
        $id = User::find($id);
        return view('admin.UserDetails',compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    //Edit form of user details
    public function edit($id)
    {  
        $id = User::find($id);
        return view('admin.EditUser',compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    // Update user data.
    public function update(Request $request, $id)
    {
        
        $input = $request->all();
        $id = User::find($id);
        $id->name = $input['name'];
        $id->email = $input['email'];
        $id->save();
        return redirect()->route('index')->with('success','Data Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    //Delete user.
    public function destroy($id)
    {
        $id = User::find($id);
        $id->delete();
        return redirect()->route('index')->with('success','Data Deleted');
    }


    // It is for reset password and change password.
    public function resetpassword($id , $email)
    {
        $id = User::find($id);
        return view('auth.passwords.resetpassword',compact('id' , 'email'));
    }

    public function changepassword(Request $request)
    {
        $this->validate($request , [
            'email' => 'required',
            'password' => ['required', 'string', 'min:8'],
            'password_confirmation' => 'required|same:password',
            ]);
        $user = User::where('email',$request['email'])->first();
        $user->password = Hash::make($request['password']);
        $user->save();
        return redirect()->route('index');
     }

    //sent mail with attachment
    public function Mailform()
    {
        return view('admin.MailForm');
    }
   
    public function sentmail(Request $request)
    {
        $this->validate($request,[
            'subject' => 'required|min:3',
            'email' => 'required|email',
            'message' => 'min:10',
            'image' => 'mimes:jpeg,png,jpg,gif,txt,pdf,ppt,docx,doc,xls'
        ]);

        $data = array(
            'email' => $request->email,
            'subject' => $request->subject,
            'message' => $request->message,
            'image' => $request->image,
        );


        Mail::send('admin.emailWelcome', $data , function($message) use ($data)
        {
            $message->to($data['email']);
            $message->subject($data['subject']." ".$data['message']);
            $message->attach($data['image']->getRealPath(), array(
                'as' => 'image.'. $data['image']->getClientOriginalExtension(),
                'mime' => $data['image']->getMimeType())
        );
        });

        return redirect('/sentMailForm')->with('success',"Mail Sent Successfully.");
    }


}
