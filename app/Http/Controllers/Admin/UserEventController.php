<?php

namespace App\Http\Controllers\Admin;
use App\Models\UserEvent;
use App\Models\User;
use App\Models\EventModule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Hash;
use Auth;

class UserEventController extends Controller
{

	//User Event-Module
    public function index()
    {
	    if (Auth::check()) 
	    {
	        $eventmodule = EventModule::all()->toArray();
	        $user = User::all()->toArray();
      		return view('admin.userEvent',compact('user','eventmodule'));
	  	}else
	  	{
	    	return redirect('/login');
	  	} 
    }

    // EVENT STORE IN DATABASE.

    public function store(Request $request)
    {
		$userevent = new UserEvent();
    	$userevent->event_id = $request->get('Category');
		$userevent->user_id = $request->get('name');
		$userevent->save();
		return redirect('/userEvent')->with('success','Data Added');
    }

    public function joindemo()
    {
    // 	return DB::table('user_event')
    // 		    ->join('event_module','event_module.id','user_event.event_id')
    // 		    ->join('users','users.id','user_event.user_id')
    // 		    ->select('users.name','event_module.event_name')
    // 		    ->where('user_event.event_id', '42')
    // 		    // ->groupBy('event_module.event_name')
				// ->get();

		 return DB::table('users')	
				->join('user_event','user_event.user_id','users.id')	
   				->select('users.name','user_event.event_id')
   				->where('user_event.user_id', '6')
   				->get();
    }			

   

}