<?php

namespace App\Http\Controllers\Admin;
use App\Models\EventModule;
use App\Models\UserEvent;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
use Illuminate\Support\Facades\Hash;
use Auth;


class EventController extends Controller
{
    //Event-Module
    public function index()
    {
	    if (Auth::check())
        {
            $eventmodule = EventModule::all()->toArray();
           
            return view('admin.Event',compact('eventmodule'));
	  	}
        else
        {
	    	return redirect('/login');
	  	} 
    }

    public function create()
    {
    	return view('admin.CreateEvent');
    } 

    // For storing event in database.

    public function store(Request $request)
    {

    	$this->validate($request , [
             'event_name' => 'required',
             'description' => 'required',
             'number' => ['required', 'integer', 'min:10'],
             'Address' => 'required',
             'category' => 'required',
             'image' => 'required',

            ]);

    	$eventmodule = new EventModule();
							
			$eventmodule->event_name = $request->get('event_name');
			$eventmodule->description = $request->get('description');
			$eventmodule->number = $request->get('number');
			$eventmodule->Address = $request->get('Address');
			$eventmodule->category = $request->get('category');
			
			if($request->hasfile('image')){
				$file = $request->file('image');
				$extension = $file->getClientOriginalExtension(); // getting image extension
				$filename = time().'.'.$extension;
				$file->move('uploads/event', $filename);
				$eventmodule->image = $filename;
			}else{
				return $request;
				$event->image = '';
			}

			$eventmodule->save();
			return redirect()->route('Event-Module')->with('success','Data Added');
			echo "data Added";
    }

    // For deleteing specific event.

    public function destroy($id)
    {
        $id = EventModule::find($id);
        $id->delete();
        return redirect()->route('Event-Module')->with('success','Data Deleted');
    }

    //For show edit event form. 

    public function edit($id)
    {  
        $id = EventModule::find($id);
        return view('admin.EditEvent',compact('id'));
    }

    // For update event.

     public function update(Request $request, $id, $imgid)
    {
        
        // $input = $request->all();
        $id = EventModule::find($id);
        $id->event_name = $request->get('event_name');
        $id->description = $request->get('description');
        $id->number = $request->get('number');
        $id->Address = $request->get('Address');
        $id->category = $request->get('category');
        
        // for image uploading.
        if($request->hasfile('image')){
				$file = $request->file('image');
				$extension = $file->getClientOriginalExtension(); // getting image extension
				$filename = time().'.'.$extension;
				$file->move('uploads/event', $filename);
				$id->image = $filename;
			}else{
				
				$id->image = $imgid;
			}

        $id->save();
        return redirect()->route('Event-Module')->with('success','Data Updated');
    }

    // For display event detail

    public function show($id)
    {
        $id = EventModule::find($id);
       
        return view('admin.EventDetails',compact('id'));
    }

    // for display user's name which is participet in Event.
    public function display($id)
    {
      
        $eventid = EventModule::find($id);
        $user_id = UserEvent::where('event_id',$eventid->id)->pluck('user_id');
        $user_detail = User::whereIn('id',$user_id)->get();
        foreach ($user_detail as $key => $value) {
            $user_list[] = $value->name;
        }
        return $user_list;
    }
}
