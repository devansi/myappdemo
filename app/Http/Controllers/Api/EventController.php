<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller;
use App\Models\EventModule; 
use Response;

class EventController extends Controller 
{

	// Event listing API

	public function eventDetail()
	{ 
		$event = EventModule::orderBy('event_name')->get();
        $count = count($event);
        for ($i=0; $i < $count; $i++) { 
        	$event[$i]['image'] = "http://127.0.0.1:8000/uploads/event/".$event[$i]['image'];
        }
        return Response::json(array('status'=>200,'data' =>$event, 'count'=>$count));
	}

	// Add Event API

	public function storeEvent(Request $request)
	{

		$this->validate($request , [
             'event_name' => 'required',
             'description' => 'required',
             'number' => ['required', 'numeric', 'min:10'],
             'Address' => 'required',
             'image' => 'required',
             'category' => 'required',
        ]);
		$eventmodule = new EventModule();
							
			$eventmodule->event_name = $request->get('event_name');
			$eventmodule->description = $request->get('description');
			$eventmodule->number = $request->get('number');
			$eventmodule->Address = $request->get('Address');
			$eventmodule->category = $request->get('category');
			$eventmodule->longitude = "72.5407";
			$eventmodule->latitude = "23.1013";

		if($request->hasfile('image'))
		{
			$file = $request->file('image');
			$extension = $file->getClientOriginalExtension(); // getting image extension
			$filename = time().'.'.$extension;
			$file->move('uploads/event', $filename);
			$eventmodule->image = $filename;
		}else
		{
			return $request;
			$event->image = '';
		}

			$eventmodule->save();
			return Response::json(array('status'=>200, 'message' =>"Event Inserted."));
	}


	// Update Event Api

	public function updateEvent(Request $request)
	{

		$id = $request->get('id');
		$Eventdata = EventModule::find($id);
		$data = $Eventdata;
		if($request->get('event_name') != ""){
			$Eventdata->event_name = $request->get('event_name');
		}
		else{
			$Eventdata->event_name = $data['event_name'];	
		}

		if($request->get('description') != ""){
			$Eventdata->description = $request->get('description');
		}
		else{
			$Eventdata->description = $data['description'];	
		}

		if($request->get('number') != ""){
			$this->validate($request , [
			 'number' => ['numeric'],
		]);
			$Eventdata->number = $request->get('number');
		}
		else{
			$Eventdata->number = $data['number'];	
		}

		if($request->get('Address') != ""){
			$Eventdata->Address = $request->get('Address');
		}
		else{
			$Eventdata->Address = $data['Address'];	
		}

		if($request->get('category') != ""){
			$Eventdata->category = $request->get('category');
		}
		else{
			$Eventdata->category = $data['category'];	
		}

        if($request->hasfile('image')){
				$file = $request->file('image');
				$extension = $file->getClientOriginalExtension(); // getting image extension
				$filename = time().'.'.$extension;
				$file->move('uploads/event', $filename);
				$Eventdata->image = $filename;
			}else{
				
				$Eventdata->image = $data['image'];
			}

        $Eventdata->save();
        return Response::json(array('status'=>200, 'message' =>"Event Updated."));
	}


	// Delete Event Api 

	public function deleteEvent(Request $request)
	{
        $id = $request->get('id');
		$event = EventModule::find($id);
        $event->delete();
         return Response::json(array('status'=>200,'data' =>"Event Deleted successfully."));
	}


}