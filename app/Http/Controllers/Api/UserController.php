<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller;
use App\Models\User; 
use Auth;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Validator;
use Response;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Hash;
use Notification;
use App\Notifications\ResetPassword;
use App\Notifications\RegistrationMail;

// use Illuminate\Support\Facades\Notification;

class UserController extends Controller 
{
	// User listing api

	public function userDetail()
    { 
		$user = User::orderBy('name')->get();
        $count = count($user);
		 return Response::json(array('status'=>200,'data' =>$user, 'count'=>$count));
  //       $text = json_encode($user);// Use of implode function  

  //       $CLIENT_ID = "FREE_TRIAL_ACCOUNT";
  //       $CLIENT_SECRET = "PUBLIC_SECRET";

  //       // Specify your translation requirements here:
  //       $postData = array(
  //         'fromLang' => "en",
  //         'toLang' => "ar",
  //         'text' => $text
  //       );

  //       $headers = array(
  //         'Content-Type: application/json',
  //         'X-WM-CLIENT-ID: '.$CLIENT_ID,
  //         'X-WM-CLIENT-SECRET: '.$CLIENT_SECRET
  //       );

  //       $url = 'http://api.whatsmate.net/v1/translation/translate';
  //       $ch = curl_init($url);

  //       curl_setopt($ch, CURLOPT_POST, 1);
  //       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  //       curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  //       curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));

  //       $response = curl_exec($ch);

  //       echo "Response: ".$response;
  //       curl_close($ch);

	}




	// Signup Api
	public function registers(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required', 
            'password' => 'required', 
            'password_confirmation' => 'required|same:password', 
        ]);

        if ($validator->fails()) { 
                return response()->json(['error'=>$validator->errors()], 401);            
            }
        $input = $request->all(); 
        $data = User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
            ]);
        
        if($data){
            $id = $data['email'];
            $data->notify(
                new RegistrationMail($data->id)
            );

             return response()->json(array('status' => 200, 'message'=> 'Registration Mail sent successfully!')); 
        }
                
        return response()->json(array('status' => 200, 'message'=> 'You are Sign Up successfully...')); 
    }





	//login Api

	public function logins(Request $request)
    { 
	 	 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
          
            $user = Auth::User();
          
            return response()->json(array('status' => 200, 'message'=> 'You are logged in successfully...')); 
        } 
        else{
        	return response()->json(array('status'=>500, 'message' => "Password and Username does not match....")); 
        } 
    }

    

  
	//forgot password
    
    public function forgot_passwords(Request $request)
    { 
        $user = USer::where('email',$request['email'])->first();
        if($user){

            $user->notify(
                new ResetPassword($user->id)
            );

            return response()->json(array('status' => 200, 'message'=> 'Forgot Password Mail sent successfully!')); 

        }else{
            return response()->json(array('status'=>500, 'message' => "Error while sending mail")); 

        }
    
    }





	

  
}

