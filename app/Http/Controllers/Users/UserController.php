<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Notifications\Notifiable;
use App\Notifications\UserProfileUpdateMail;

use Mail;
use App\Jobs\newDemoUserEmail;

class UserController extends Controller
{
    //User Module
	 public function index()
    {
    	if (Auth::check()){
            
            newDemoUserEmail::dispatch()->delay(now()->addMinutes(1));
    		return view('users.index');
    	}else{
    		return redirect('login');
    	}
    }

    //Display user details

    public function view($id)
    {
    	$user = User::find($id);
    	return view('users.userDetail',compact('user'));
    }

    // Edit form of user.

    public function edit($id)
    {	
    	$user = User::find($id);
    	return view('users.userUpdate',compact('user'));
    }

    //Update user details.

    public function update(Request $request , $id)
    {
    	$input = $request->all();
        $id = User::find($id);
        $id->name = $input['name'];
        $id->email = $input['email'];
        $id->save();
        $id->notify(
            new UserProfileUpdateMail($id->email)
        );
         return redirect()->route('user-index')->with('success','Data Updated');
    }

    // Change password module

    public function ChangePassword()
    {
    	if (Auth::check()){
    		return view('users.userChangePassword');
    	}else{
    		return redirect('/login');
    	}
    }

    //Update password of user.

    public function UpdatePassword(Request $request)
    {
    	if (!(Hash::check($request->get('old-password'), Auth::user()->password))) {
    		// The passwords matches
            return redirect('User-Change-Password')->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->get('old-password'), $request->get('password')) == 0)
        {
            //Current password and new password are same
            return redirect('User-Change-Password')->with("error","New Password cannot be same as your current password. Please choose a different password.");
       	}

       	// for password validation
       	$this->validate($request,[
            'old-password' => 'required',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|same:password',
            
        ]);

        $user = Auth::user();
        $user->password =Hash::make($request->get('password'));
        $user->save();

       	 return redirect('user-index')->with("success","Password changed successfully !");

    }


}