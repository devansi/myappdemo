<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\EventActivity;
use App\Models\userAttendyActivity;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Mail;
use Auth;
use App\Notifications\InvoiceMailSent;
use Illuminate\Support\Facades\Notification;
use PDF;
use App\Models\EventTiming;
use DB;




class UserEventActivityController extends Controller
{
    //Event User Module
	public function index()
    {
    	if (Auth::check()){
    		$eventname = EventActivity::all()->pluck('event_name','event_id');
    		return view('users.UserEventForm',compact('eventname'));
    	}else{
    		return redirect('login');
    	}
    }

    public function getdropdowndata($event_id)
    {
    	$data = EventActivity::where('event_id','=', $event_id)->get();
    	return$data;
    }

    public function getdropdowndataprice($id)
    {
    	$data = EventActivity::where('id','=', $id)->get('price');
    	return$data;
    }

    public function store(Request $request)
    {
    	$userattendyactivity = new userAttendyActivity();
    	$userattendyactivity->user_id = Auth::user()->id;
    	$userattendyactivity->event_id = $request->get('event');
    	$userattendyactivity->activity_id = $request->get('activity');
    	$userattendyactivity->number_of_person = $request->get('number');

    	$price = $request->get('price');
    	if($price != "")
    	{
    		$actullyPrice = ($userattendyactivity->number_of_person)*$price;
    		$userattendyactivity->total_price = $actullyPrice;
    	}
    	else{
    		echo "Price are not displaied";
    	}
    	$userattendyactivity->save();
    	$date = userAttendyActivity::get()->last();
    	$srno = $date->id;
    	$datee = $date->created_at;
    	$tax = $date->tax;
    	
    	$aid = $userattendyactivity->activity_id;
    	$aname = EventActivity::where('id','=', $aid)->get('activity_name');
    	$activityname = explode(':',$aname);
    	$activityname = explode('}',$activityname[1]);

    	$ename = EventActivity::where('id','=', $aid)->get('event_name');
    	$eventname = explode(':',$ename);
    	$eventname = explode('}',$eventname[1]);

    	$person = $userattendyactivity->number_of_person;

    	$finalamount = $actullyPrice + $tax;
    

    	$data[] = [$activityname[0],$person,$price,$eventname[0],$actullyPrice,$finalamount,$tax,$srno,$datee];

    	
    	//this is new method to sent mail.
    	$user = Auth::user();
		$user->notify(
		    new InvoiceMailSent($data)
		);

    	//this is old method to sent mail
    	// Mail::send('users.invoice',["data1"=>$data],function($message) use ($data)
     //    {
     //        $message->to(Auth::user()->email);
     //        $message->subject("Invoice of Event");
     //     });

    	

		return redirect()->route('EventActivityForm')->with('success','Invoice Sent in your mail.');
    
    	
    	
    }

    public function TimeslotForm()
    {
        if (Auth::check()){
            $eventname = EventActivity::all()->pluck('event_name','event_id');
            return view('users.timeslot',compact('eventname'));
        }else{
            return redirect('login');
        }
    }

    public function StoreTimeslot(Request $request)
    {
        $EventTiming = new EventTiming();
        $activity_id = $request->get('activity');
        $start_time = $request->get('timeslot');
        $dayname = array_keys($start_time);
        $times = array_values($start_time);
         $insert = [];
        for ($index = 0; $index < count($dayname); $index ++) {
            if($dayname[$index] == 1)
           {
            $day = "Monday";
           }elseif($dayname[$index] == 2)
           {
             $day = "Tuesday";
           }
           elseif($dayname[$index] == 3)
           {
             $day = "Wednesday";
           }
           elseif($dayname[$index] == 4)
           {
             $day = "Thursday";
           }
           elseif($dayname[$index] == 5)
           {
             $day = "Friday";
           }
           elseif($dayname[$index] == 6)
           {
             $day = "Saturday";
           }
           elseif($dayname[$index] == 7)
           {
             $day = "Sunday";
           }

           $Times = explode("-",$times[$index]);
           $starttime = date("G:i", strtotime($Times[0]));
           $endtime = date("G:i", strtotime($Times[1]));
          

           $data = [
           'activity_id' => $activity_id,
           'day' => $day,
           'start_time' => $starttime,
           'end_time' => $endtime,
           ];
         $insert[] = $data;
        }
        DB::table('event_timing')->insert($insert);
        return redirect()->route('Timeslot')->with('success','Data Added.');
    }
    
}
