<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\User;
use Auth;

class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {   
        //if it is not admin, loggedin.
        if(Auth::User()->userType == 2)
        {
            return $next($request);
            return $response->header('Cache-control','nocache, no-store, max-age=0, must-revalidate')
                            ->header('pragma','no-cache')
                            ->header('Expires','Fri, 01 Jan 1990 00:00:00 GMT');
        }
        else{
            return redirect('/login');
        }
       
    }
}
