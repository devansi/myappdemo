<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventModuleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_module', function (Blueprint $table) {
            $table->id();
            $table->string('event_name');
            $table->longtext('description');
            $table->string('number');
            $table->string('Address');
            $table->string('image');
            $table->string('category'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_module');
    }
}
